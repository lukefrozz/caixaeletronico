/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caixaeletronicojavaconsole.domains;

/**
 * classe representativa de uma Cédula
 * @author Luke Frozz
 * @since 09/09/2015
 */
public class Cedula {
    /**
     * Valor da Cédula
     */
    private int valor;

    /**
     * Getter de valor
     * @return Valor da Cédula
     */
    public int getValor() {
        return valor;
    }
    /**
     * Setter de valor
     * @param valor atribui o valor da cédula
     */
    public void setValor(int valor) {
        this.valor = valor;
    }

    /**
     * Constutor padrão de Cédula
     */
    public Cedula() {
    }
    /**
     * Construtor completo de Cédula
     * @param valor atribui o valor da cédula
     */
    public Cedula(int valor) {
        this.valor = valor;
    }
    
}
