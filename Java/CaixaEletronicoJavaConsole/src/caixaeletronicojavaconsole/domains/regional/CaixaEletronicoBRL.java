/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caixaeletronicojavaconsole.domains.regional;

import caixaeletronicojavaconsole.domains.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe representativa do Caixa Eletronico Brasileiro
 * @author Luke Frozz
 * @since 09/09/2015
 */
public class CaixaEletronicoBRL extends CaixaEletronico {

    // <editor-fold default-state="collapsed" desc="Métodos">
    @Override
    public void saque(int valor) {
        int auxValor = valor;
        List<Slot> auxSlots = setDefaultSlotsByRegional(0);
        
        while (slots.get(5).getQuantidade() > 0 && auxValor >= 100) {
            auxValor -= 100;
            auxSlots.get(5).setQuantidade(auxSlots.get(5).getQuantidade() + 1);
            slots.get(5).setQuantidade(slots.get(5).getQuantidade() - 1);
        }

        while (slots.get(4).getQuantidade() > 0 && auxValor >= 50) {
            auxValor -= 50;
            auxSlots.get(4).setQuantidade(auxSlots.get(4).getQuantidade() + 1);
            slots.get(4).setQuantidade(slots.get(4).getQuantidade() - 1);
        }

        while (slots.get(3).getQuantidade() > 0 && auxValor >= 20) {
            auxValor -= 20;
            auxSlots.get(3).setQuantidade(auxSlots.get(3).getQuantidade() + 1);
            slots.get(3).setQuantidade(slots.get(3).getQuantidade() - 1);
        }

        while (slots.get(2).getQuantidade() > 0 && auxValor >= 10) {
            auxValor -= 10;
            auxSlots.get(2).setQuantidade(auxSlots.get(2).getQuantidade() + 1);
            slots.get(2).setQuantidade(slots.get(2).getQuantidade() - 1);
        }

        while (slots.get(1).getQuantidade() > 0 && auxValor >= 5 && (auxValor % 5) % 2 == 0) {
            auxValor -= 5;
            auxSlots.get(1).setQuantidade(auxSlots.get(1).getQuantidade() + 1);
            slots.get(1).setQuantidade(slots.get(1).getQuantidade() - 1);
        }

        while (slots.get(0).getQuantidade() > 0 && auxValor >= 2) {
            auxValor -= 2;
            auxSlots.get(0).setQuantidade(auxSlots.get(0).getQuantidade() + 1);
            slots.get(0).setQuantidade(slots.get(0).getQuantidade() - 1);
        }

        int verificador = 0;
        
        for (Slot slot : auxSlots)
            verificador += slot.getQuantidade() * slot.getCedula().getValor();
        
        if (verificador != valor) {
            System.out.println("Valor indisponível para Saque");
        } else {
            StringBuilder text = new StringBuilder();
            
            for (Slot slot : auxSlots)
                if (slot.getQuantidade() > 0)
                    text.append(String.format(
                        "| %s - %3s%s\n", 
                        (String.format("R$ %3s,00",
                            slot.getCedula().getValor())), 
                        slot.getQuantidade(), "|"));
            System.out.println(text);
        }
    }

    @Override
    public void deposito(int valor) {
        
    }

    @Override
    protected List<Slot> setDefaultSlotsByRegional(int quantidade) {
        List<Slot> slotsReturn = new ArrayList<>();
        slotsReturn.add(new Slot(new Cedula(2),   quantidade));
        slotsReturn.add(new Slot(new Cedula(5),   quantidade));
        slotsReturn.add(new Slot(new Cedula(10),  quantidade));
        slotsReturn.add(new Slot(new Cedula(20),  quantidade));
        slotsReturn.add(new Slot(new Cedula(50),  quantidade));
        slotsReturn.add(new Slot(new Cedula(100), quantidade));
        return slotsReturn;
    }// </editor-fold>
    
    // <editor-fold default-state="collapsed" desc="Construtores">
    /**
     * Construtor completo de CaixaEletronicoBRL
     */
    public CaixaEletronicoBRL() {
        super();
    }
    /**
     * Construtor completo de CaixaEletronicoBRL
     * @param quantidade atribui a quantidade de Cédulas dos Slots
     */
    public CaixaEletronicoBRL(int quantidade) {
        super(quantidade);
    }// </editor-fold>
    
}
