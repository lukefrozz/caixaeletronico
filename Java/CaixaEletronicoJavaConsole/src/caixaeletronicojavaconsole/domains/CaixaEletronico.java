/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caixaeletronicojavaconsole.domains;

import java.util.*;

/**
 * Classe representativa de um Caixa Eletrônico
 * @author Luke Frozz
 * @since 09/09/2015
 */
public abstract class CaixaEletronico {
    /**
     * Coleção de Slots do Caixa Eletrônico
     */
    protected List<Slot> slots;
    
    /**
     * Método de saque
     * @param valor Valor do saque
     */
    public abstract void saque(int valor);
    /**
     * Método de depósito
     * @param valor Valor do depósito
     */
    public abstract void deposito(int valor);
    /**
     * Método uilizado para a criação de slots padrão por região;
     */
    protected abstract List<Slot> setDefaultSlotsByRegional(int quantidade);
    
    /**
     * Construtor padrão de CaixaEletronico
     */
    public CaixaEletronico() {
        slots = setDefaultSlotsByRegional(100);
    }
    /**
     * Construtor completo de CaixaEletronico
     * @param quantidade atribui a quantidade de Cédulas nos Slots do CaixaEletronico
     */
    public CaixaEletronico(int quantidade) {
        slots = setDefaultSlotsByRegional(quantidade);
        for(Slot slot : slots) {
            slot.setQuantidade(quantidade);
        }
    }
}
