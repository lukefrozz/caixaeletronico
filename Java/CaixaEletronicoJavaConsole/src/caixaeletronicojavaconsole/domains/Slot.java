/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caixaeletronicojavaconsole.domains;

/**
 * Classe representativa de um Slot
 * @author Luke Frozz
 * @since 09/09/2015
 */
public class Slot {
    /**
     * Cédula do Slot
     */
    private Cedula cedula;
    /**
     * Quantidade de Cédulas do Slot
     */
    private int quantidade;

    /**
     * Getter de Cédula
     * @return Cédula do slot
     */
    public Cedula getCedula() {
        return cedula;
    }
    /**
     * Setter de Cédula
     * @param cedula atribui a cédula do Slot
     */
    public void setCedula(Cedula cedula) {
        this.cedula = cedula;
    }
    /**
     * Getter de quantidade
     * @return quantidade de Cédulas do Slot
     */
    public int getQuantidade() {
        return quantidade;
    }
    /**
     * Setter de quantidade
     * @param quantidade atribui a quantidade de cédulas do Slot
     */
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    /**
     * Constutor Padrão de Slot
     */
    public Slot() {
    }

    /**
     * Construtor completo de Slot onde
     * @param cedula atribui a cédula de Slot
     * @param quantidade atribui a quantidade de Slot
     */
    public Slot(Cedula cedula, int quantidade) {
        this.cedula = cedula;
        this.quantidade = quantidade;
    }
    
}
